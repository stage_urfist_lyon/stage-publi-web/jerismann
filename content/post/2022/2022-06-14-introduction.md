---
title: "Découvrez mon profil atypique"
date: 2022-06-14T10:58:38+02:00
draft: false
---

## De la géographie à l'informatique

*Le parallèle n'est pas si difficile à faire*

Comme vous pouvez le constater dans mon parcours

         code de citation
```
du code
```
De la Géographie

![De la Géo](/fdmy68.jpeg)

A l'informatique

![à l'informatique](/carte-reseau-informatique.jpeg)

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
